export function itemsHasErrored(bool) {
  return {
    type: "ITEMS_HAS_ERRORED",
    hasErrored: bool
  };
}

export function itemsFetchDataSuccess(items) {
  return {
    type: "ITEMS_FETCH_DATA_SUCCESS",
    items
  };
}

export function updateItems(item) {
  return {
    type: "ITEMS_UPDATED",
    item
  };
}

// thunk action
export function itemsFetchData(url) {
  return dispatch => {

    fetch(url)
      .then(response => response.json())
      .then(response => {
        return response.map(el => ({
            index: el.index,
            NASDAQ: el.stocks.NASDAQ.toFixed(2),
            CAC40: el.stocks.CAC40.toFixed(2),
          }));
      })
      .then(items => dispatch(itemsFetchDataSuccess(items)))
      .catch(() => dispatch(itemsHasErrored(true)));
  };
}
