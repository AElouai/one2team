export function itemsHasErrored(state = false, action) {
  switch (action.type) {
    case "ITEMS_HAS_ERRORED":
      return action.hasErrored;

    default:
      return state;
  }
}

export function items(state = [], action) {
  switch (action.type) {
    case "ITEMS_FETCH_DATA_SUCCESS":
      const lastItem = action.items[action.items.length - 1];
      if (state.length > 0) {
        if (state.length >= 20) state.shift();
        return [...state, lastItem];
      }
      return [lastItem];
    case "ITEMS_UPDATED":
      const foundIndex = state.findIndex(x => x.index === action.item.index);
      state[foundIndex] = action.item;
      return [...state];

    default:
      return state;
  }
}
