/*
 src/reducers/rootReducer.js
*/
import { combineReducers } from "redux";
import { items, itemsHasErrored } from "./items";

export default combineReducers({
  items,
  itemsHasErrored
});
