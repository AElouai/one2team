import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { itemsFetchData, updateItems } from "../../store/actions/items";
import LineChart from "../LineChart";
import Table from "../Table";

import "./App.css";

class App extends Component {
  componentDidMount() {
    const { fetchData } = this.props;
    fetchData();
    setInterval(fetchData, 1000);
  }
  
  render() {
    const {
      items,
      hasErrored,
      updateItems
    } = this.props;

    if (hasErrored) return <p>Sorry! There was an error loading the data , please refresh your browser</p>;

    return (
      <div className="App">
        <LineChart
          items={items}
          title={"le cours du CAC40 et du NASDAQ"}
        />
        <Table items={items} handleChange={updateItems} />
      </div>
    );
  }
}

App.propTypes = {
  fetchData: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  hasErrored: PropTypes.bool.isRequired
};

const mapStateToProps = state => {
  return {
    items: state.items,
    hasErrored: state.itemsHasErrored
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateItems :(elment)=> dispatch(updateItems(elment)),
    fetchData: (url='http://127.0.0.1:8000?count=20') => dispatch(itemsFetchData(url))
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(App);
