import React from "react";
import { Line } from "react-chartjs-2";

const LineChart = ({ title, items = [] }) => {
  const NASDAQ = items.map(el => el.NASDAQ);
  const CAC40 = items.map(el => el.CAC40);
  const data = {
    labels: [...Array(items.length).keys()],
    datasets: [
      {
        data: NASDAQ,
        label: "NASDAQ",
        borderColor: "orange",
        fill: false
      },
      {
        data: CAC40,
        label: "CAC40",
        borderColor: "blue",
        fill: false
      }
    ]
  };

  return (
    <div>
      <h2>{title}</h2>
      <Line data={data} />
    </div>
  );
};

export default LineChart;
