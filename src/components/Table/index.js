import React from "react";
import { throttle } from "throttle-debounce";
import "./table.css";

const TableRow = ({ items, handleChangeThrottled, type }) =>
  items.map((el, index) => (
    <td key={`${el.index}-${type}-${index}`}>
      <input
        value={el[type]}
        onChange={event =>
          handleChangeThrottled({ ...el, [type]: event.target.value })
        }
      />
    </td>
  ));

const Table = ({ items = [], handleChange }) => {
  const handleChangeThrottled = throttle(1500, handleChange);

  return (
    <table>
      <tbody>
        <tr>
          <th>NASDAQ</th>
          <TableRow
            items={items}
            type={"NASDAQ"}
            handleChangeThrottled={handleChangeThrottled}
          />
        </tr>
        <tr>
          <th>CAC40</th>
          <TableRow
            items={items}
            type={"CAC40"}
            handleChangeThrottled={handleChangeThrottled}
          />
        </tr>
      </tbody>
    </table>
  );
};

export default Table;
